import Boot from 'scripts/states/boot.js';
import Preload from 'scripts/states/preload.js';
import Menu from 'scripts/states/menu.js';
import Evolution from 'scripts/states/evolution.js';
import Credits from 'scripts/states/credits.js';
import Game from 'scripts/states/game.js';

class Games extends Phaser.Game {
    constructor() {
        super({
            width: window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth,
            height: window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight,
            transparent: false,
            enableDebug: true
        });

        this.state.add('boot', Boot);
        this.state.add('preload', Preload);
        this.state.add('menu', Menu);
        this.state.add('evolution', Evolution);
        this.state.add('credits', Credits);
        this.state.add('game', Game);

        this.state.start('boot');
    }
}

export default Games;