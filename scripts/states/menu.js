'use strict';

class Menu {

    create() {
        this.game.stage.backgroundColor = '#FFF';

        var tmpMenuLogo = this.game.cache.getImage('menu_logo');
        var menu_logo = this.game.add.sprite(
            (this.game.width - tmpMenuLogo.width) / 2,
            this.game.height * 0.2,
            'menu_logo'
        );


        var tmpMenuStart = this.game.cache.getImage("menu_start");
        var menu_start_y = (this.game.height - tmpMenuStart.height) / 2;
        var menu_start_x = (this.game.width - tmpMenuStart.width) / 2;
        var menu_start = this.game.add.sprite(
            menu_start_x,
            menu_start_y,
            'menu_start');
        menu_start.inputEnabled = true;
        menu_start.events.onInputDown.add(this.goEvolution, this);

        var tmpMenuCredits = this.game.cache.getImage("menu_credits");
        var menu_credits = this.game.add.sprite(
            menu_start_x,
            1.5 * tmpMenuStart.height + menu_start_y,
            'menu_credits');
        menu_credits.inputEnabled = true;
        menu_credits.events.onInputDown.add(this.goCredits, this);

        var tmpMenuDarwin = this.game.cache.getImage("menu_darwin");
        var menu_darwin_x = this.game.width - tmpMenuDarwin.width - 0.1 * this.game.width;
        var menu_darwin_y = this.game.height - tmpMenuDarwin.height - 0.1 * this.game.height;
        var menu_darwin = this.game.add.sprite(menu_darwin_x, menu_darwin_y, "menu_darwin");

    }

    goCredits() {
        this.game.state.start('credits');
    }

    goGame() {
        this.game.state.start('game');
    }

    goEvolution() {
        this.game.state.start('evolution');
    }
}

export default Menu;