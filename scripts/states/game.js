'use strict';

import DarwinianNetwork from 'scripts/class/darwiniannetwork.js';
import {generateRandomColor, generateRandomTraitColors} from 'scripts/utility.js';

class Game {

    preload() {
        // Load plugin for UI
        this.game.slickUI = this.game.plugins.add(Phaser.Plugin.SlickUI);
        this.game.slickUI.load('assets/ui/kenney/kenney.json');
    }

    create() {

        // Initialize physics
        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        // Game scene
        this.game.stage.backgroundColor = '#000';
        this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'grid');

        // Sprite for operators
        this.game.populationOperators = {};
        this.game.populationOperators["remove"] = this.game.add.sprite(0, 0, "removeIcon");
        this.game.physics.arcade.enable(this.game.populationOperators["remove"])
        this.game.populationOperators["remove"].body.moves = false;
        this.game.populationOperators["replicate"] = this.game.add.sprite(this.game.width-50, 0, "replicateIcon");
        this.game.physics.arcade.enable(this.game.populationOperators["replicate"]);
        this.game.populationOperators["replicate"].body.moves = false;


        // Generate DN
        var traits = ["a", "b"];
        var population_params = [
            {
                "combative": ["a"],
                "docile": []
            },
            {
                "combative": ["b"],
                "docile": ["a"]
            },
        ];
        var popColor = "white";
        var traitColors = generateRandomTraitColors(traits);
        this.game.dn = new DarwinianNetwork(this.game, population_params, traitColors, popColor);
    }

    update() {
        this.game.dn.checkCollisions();
    }

}

export default Game;