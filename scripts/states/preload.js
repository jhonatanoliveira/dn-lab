'use strict';

class Preload {
    preload() {

        // Add preload sprite
        var tmpPreload = this.game.cache.getImage('preloader');
        this.loadingSprite = this.add.sprite(
            (this.game.width - tmpPreload.width) / 2,
            (this.game.height - tmpPreload.height) / 2,
            'preloader'
        );

        // run preload sprite
        this.load.onLoadComplete.addOnce(this.onLoadComplete, this);
        this.load.setPreloadSprite(this.loadingSprite);

        // Menu Assets
        this.load.image('menu_logo', 'assets/menu/logo.png');
        this.load.image('menu_credits', 'assets/menu/main_menu_credits.png');
        this.load.image('menu_start', 'assets/menu/main_menu_start.png');
        this.load.image('menu_darwin', 'assets/menu/main_menu_darwin.png');

        // Credits Assets
        this.load.image('credits_text', 'assets/credits/credits_text.png');
        this.load.image('credits_darwin', 'assets/credits/credits_darwin.png');


        this.load.image('grid', 'assets/grid.jpeg');
        this.load.image('removeIcon', 'assets/removeIcon.png');
        this.load.image('replicateIcon', 'assets/replicateIcon.png');

        this.game.time.advancedTiming = true;
    }

    onLoadComplete() {
        this.game.state.start('menu', true, false);
    }
}

export default Preload;