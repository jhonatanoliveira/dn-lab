'use strict';

class Evolution {

    create() {
        this.game.stage.backgroundColor = '#FFF';

        var tmpMenuLogo = this.game.cache.getImage('menu_logo');
        var menu_logo = this.game.add.sprite(
            (this.game.width - tmpMenuLogo.width) / 2,
            this.game.height * 0.2,
            'menu_logo'
        );
    }
}

export default Evolution;