export function generateRandomColor(n){
    n = n || 127;
    var r = (Math.round(Math.random()* n) + n).toString(16);
    var g = (Math.round(Math.random()* n) + n).toString(16);
    var b = (Math.round(Math.random()* n) + n).toString(16);
    return '#' + r + g + b;
}

export function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function generateRandomTraitColors(traits) {
	var traitColors = {};
    var generatedColors = [];
    for (var i = traits.length - 1; i >= 0; i--) {
        var color = generateRandomColor();
        while (generatedColors.indexOf(color) != -1){
            color = generateRandomColor();
        }
        generatedColors.push(color);
        traitColors[traits[i]] = color;
    }
    return traitColors;
}

Set.prototype.isSuperset = function(subset) {
    for (var elem of subset) {
        if (!this.has(elem)) {
            return false;
        }
    }
    return true;
}

Set.prototype.union = function(setB) {
    var union = new Set(this);
    var setB = Array.from(setB);
    for (var i = setB.length - 1; i >= 0; i--) {
        union.add(setB[i]);
    }
    return union;
}

Set.prototype.intersection = function(setB) {
    var intersection = new Set();
    var setB = Array.from(setB);
    for (var i = setB.length - 1; i >= 0; i--) {
        if (this.has(setB[i])) {
            intersection.add(setB[i]);
        }
    }
    return intersection;
}

Set.prototype.difference = function(setB) {
    var difference = new Set(this);
    var setB = Array.from(setB);
    for (var i = setB.length - 1; i >= 0; i--) {
        difference.delete(setB[i]);
    }
    return difference;
}