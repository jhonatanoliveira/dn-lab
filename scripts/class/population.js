class Population {

    constructor(game, combative, docile, traitColors, popColor, popX, popY) {
        // Constants
        this.TRAIT_RADIUS = 20;
        this.TRAIT_OUTER_CIRCLE_PERCENTAGE = 0.6;
        this.TRAIT_STROKE = "#282828";
        this.TRAIT_STROKE_WIDTH = this.TRAIT_RADIUS * 0.4;
        this.TRAIT_POPUP_RADIUS = this.TRAIT_RADIUS * 3
        // Initializations
        this.game = game;
        this.combative = combative;
        this.docile = docile;
        this.traitColors = traitColors;
        this.popX = popX;
        this.popY = popY;
        // Population Properties
        var nTraits = this.combative.length + this.docile.length;
        if (nTraits==1) {nTraits=2} // nTrait 1 can't be computed in function sin
        this.radius =  this.TRAIT_RADIUS*(1+Math.sin(Math.PI/nTraits))/Math.sin(Math.PI/nTraits) * (1+this.TRAIT_OUTER_CIRCLE_PERCENTAGE);
        this.color = popColor;
        this.generatePopulationSprite();
    }

    generatePopulationSprite(){
        var popDrawing = this.drawPop(this.radius, this.color);
        this.sprite = this.game.add.sprite(this.x, this.y, popDrawing);
        this.sprite.populationRef = this;

        this.sprite.inputEnabled = true;
        this.sprite.input.enableDrag();
        this.game.physics.arcade.enable([this.sprite]);
        this.sprite.body.moves = false;

        this.sprite.x = this.popX || this.game.world.randomX;
        if ((this.sprite.x + this.radius) > this.game.world.width) {
            this.sprite.x -= 2*this.radius;
        } else if ((this.sprite.x - this.radius) < 0) {
            this.sprite.x += 2*this.radius;
        }
        this.sprite.y = this.popY || this.game.world.randomY;
        if ((this.sprite.y + this.radius) > this.game.world.height) {
            this.sprite.y -= 2*this.radius;
        } else if ((this.sprite.y - this.radius) < 0) {
            this.sprite.y += 2*this.radius;
        }
        
    }

    generatePopupCombative() {
        // Generate area of click outside big traits
        var areaClickSprite = this.game.add.sprite(0, 0);
        areaClickSprite.scale.x = this.game.width;
        areaClickSprite.scale.y = this.game.height;
        // Generate big traits
        var popupSize = 2 * this.TRAIT_POPUP_RADIUS * this.combative.length;
        var popupPopSprites = [];
        var y = this.game.height/2;
        for (var i = 0; i < this.combative.length; i++) {
            var bitmapSize = 2 * this.TRAIT_POPUP_RADIUS;
            var bmd = this.game.add.bitmapData(bitmapSize, bitmapSize);
            var c = this.combative[i];
            this.drawCircle(bmd,this.TRAIT_POPUP_RADIUS,this.TRAIT_POPUP_RADIUS,this.TRAIT_POPUP_RADIUS, this.traitColors[c]);
            var pSprite = this.game.add.sprite((this.game.width- popupSize)/2+i*(2*this.TRAIT_POPUP_RADIUS),y,bmd);
            pSprite.populationRef = this;
            pSprite.traitRef = c;
            pSprite.areaClickSpriteRef = areaClickSprite;
            pSprite.popupPopSpritesRef = popupPopSprites;
            pSprite.inputEnabled = true;
            pSprite.events.onInputDown.add(this.game.dn.performReplication, this);
            popupPopSprites.push(pSprite);
        }
        // Detect click outside traits
        areaClickSprite.inputEnabled = true;
        areaClickSprite.popupPopSpritesRef = popupPopSprites;
        areaClickSprite.events.onInputDown.add(this.destroyPopup, this);
        return popupPopSprites;
    }

    destroyPopup(areaClickSprite) {
        var popupPopSprites = areaClickSprite.popupPopSpritesRef;
        //### DEBUG
        console.log(">>.... here")
        //###---DEBUG
    }

    drawPop(radius, color){
        var bitmapSize = 2 * radius;
        var bmd = this.game.add.bitmapData(bitmapSize, bitmapSize);

        // Draw background
        this.drawCircle(bmd, radius, radius, radius, color);
        // Draw traits
        var traitAngleShift = 360 / (this.combative.length + this.docile.length);
        var curr_trait_angle = 0;
        for (var i = this.combative.length - 1; i >= 0; i--) {
            var c = this.combative[i];
            this.drawTrait(bmd, radius, radius, radius, curr_trait_angle * traitAngleShift, this.traitColors[c]);
            curr_trait_angle++;
        }
        for (var i = this.docile.length - 1; i >= 0; i--) {
            var c = this.docile[i];
            this.drawTrait(bmd, radius, radius, radius, curr_trait_angle * traitAngleShift, this.TRAIT_STROKE, this.traitColors[c]); // reverse color of docile
            curr_trait_angle++;
        }


        return bmd;
    }

    drawTrait(bmd, popX, popY, dnR, degree, color, stroke) {
        var traitAngle = -(degree * Math.PI)/180;
        var traitOuterCircle = this.TRAIT_OUTER_CIRCLE_PERCENTAGE * dnR;
        // A point at angle theta on the circle whose centre is (x0,y0)
        // and whose radius is r is (x0 + r cos theta, y0 + r sin theta).
        // Now, choose theta values evenly spaced between 0 and 2pi.
        // http://stackoverflow.com/questions/5300938/calculating-the-position-of-points-in-a-circle
        var x = popX + traitOuterCircle * Math.cos(traitAngle);
        var y = popY + traitOuterCircle * Math.sin(traitAngle);
        var r = this.TRAIT_RADIUS;
        this.drawCircle(bmd, x, y, r, color, stroke)
    }

    drawCircle(bmd, x, y, r, color, stroke) {
        bmd.ctx.fillStyle = color;
        bmd.ctx.beginPath();
        bmd.ctx.arc(x, y, r, 0, 2*Math.PI);
        bmd.ctx.closePath();
        bmd.ctx.fill();
        if (stroke != undefined) {
            bmd.ctx.lineWidth=this.TRAIT_STROKE_WIDTH;
            bmd.ctx.strokeStyle=stroke;
            bmd.ctx.stroke();
        }
    }
}

export default Population;
