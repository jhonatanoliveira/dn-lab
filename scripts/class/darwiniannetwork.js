'use strict';

import Population from 'scripts/class/population.js';

class DarwinianNetwork {

    constructor(game, population_params, traitColors, popColor){
    	this.history = []
    	this.game = game;
    	this.populations = [];
    	this.traitColors = traitColors;
    	this.popColor = popColor;
    	for (var i = population_params.length - 1; i >= 0; i--) {
    		var population_param = population_params[i];
    		var population = new Population(this.game, population_param["combative"], population_param["docile"], this.traitColors, this.popColor);
    		this.populations.push(population)
    	}
    }

    checkCollisions(){
    	for (var i = 0; i < this.populations.length; i++) {
            var population1Sprite =  this.populations[i].sprite;

            // Checl ollision with operators
            this.game.physics.arcade.collide(population1Sprite, this.game.populationOperators["replicate"], this.handlePopulationReplicate, null, this);
            // Check collision with populations
            for (var j = 0; j < this.populations.length; j++) {
                var population2sprite = this.populations[j].sprite;
                this.game.physics.arcade.collide(population1Sprite, population2sprite, this.handlePopulationMerge, null, this);
            }

        }
    }

    handlePopulationReplicate(popSprite, operatorSprite) {
        var population = popSprite.populationRef || operatorSprite.populationRef;
        if (population.combative.length > 1) {
            var popupSprites = population.generatePopupCombative();
            population.sprite.visible = false;
            population.sprite.body.enable = false;
        }
    }

    performReplication(bigTraitSprite) {
        var population = bigTraitSprite.populationRef;
        var trait = bigTraitSprite.traitRef;
        var areaClickSprite = bigTraitSprite.areaClickSpriteRef;
        var popupPopSprites = bigTraitSprite.popupPopSpritesRef;
        // remove population from DN
        this.game.dn.removePopulation(population);
        this.game.dn.history.push({"op": "replication", "pop": population, "trait": trait});
        // Generate new population
        var combRepPop = [];
        for (var i = population.combative.length - 1; i >= 0; i--) {
            if (population.combative[i] != trait) {
                combRepPop.push(population.combative[i]);
            }
        }
        var mergedPopulation = new Population(this.game, combRepPop, population.docile.slice(), this.game.dn.traitColors, this.game.dn.popColor, population.sprite.x, population.sprite.y );
        // Remove big traits and outside area of click
        areaClickSprite.destroy();
        for (var i = popupPopSprites.length - 1; i >= 0; i--) {
            popupPopSprites[i].destroy();
        }
    }

    handlePopulationMerge(pop1, pop2){
        // Merge condition
        var pop1 = pop1.populationRef;
        var pop2 = pop2.populationRef;
    	if ((pop1.combative.length > 0 || pop2.combative.length > 0) && (new Set(pop1.combative).intersection(new Set(pop2.combative)).size == 0)) {
    		this.history.push({"op": "merge", "pop1": pop1, "pop2": pop2});
    		// remove populations from DN
            this.removePopulation(pop1);
    		this.removePopulation(pop2);
    		// Compute new traits
    		var combPop1 = new Set(pop1.combative);
    		var combPop2 = new Set(pop2.combative);
    		var combMergPop = combPop1.union(combPop2)
    		var docPop1 = new Set(pop1.docile);
    		var docPop2 = new Set(pop2.docile);
    		var docMergPop = docPop1.union(docPop2).difference(combMergPop);
    		// Draw new population
    		var xPos =  (pop1.sprite.x + pop2.sprite.x)/2;
    		var yPos =  (pop1.sprite.y + pop2.sprite.y)/2;
    		var mergedPopulation = new Population(this.game, Array.from(combMergPop), Array.from(docMergPop), this.traitColors, this.popColor, xPos, yPos	);
    		// Save population in DN
    		this.populations.push(mergedPopulation)
    	}
    }

    removePopulation(pop) {
        this.populations.splice(this.populations.indexOf(pop),1);
        pop.sprite.visible = false;
        pop.sprite.body.enable = false;
    }

}

export default DarwinianNetwork