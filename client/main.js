// Templates
import '../imports/ui/layout.js';
import '../imports/ui/home.js';

import '../imports/ui/dns/dns.js';
import '../imports/ui/dns/editDn.js';
import '../imports/ui/dns/drawDn.js';

import '../imports/ui/evolutions/evolutions.js';
import '../imports/ui/evolutions/playEvolution.js';
import '../imports/ui/evolutions/viewEvolution.js';