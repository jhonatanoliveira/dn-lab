Router.configure({
  layoutTemplate: 'layout'
});



Router.route("/", function() {
  this.render("home");
}, {name: "home"})

Router.route("/dns", function() {
  this.wait([Meteor.subscribe("allDarwinianNetworks"), Meteor.subscribe("allPopulations")]);
  this.render("dns");
}, {name: "dns"})

Router.route("/evolutions", function() {
  this.wait([Meteor.subscribe("allEvolutions")]);
  this.render("evolutions");
}, {name: "evolutions"});



import { DarwinianNetworks } from "../imports/api/darwiniannetworks.js";

Router.route("/dns/edit/:_id", function() {
  var dnId = this.params._id;
  this.wait([Meteor.subscribe("darwinianNetwork", dnId), Meteor.subscribe("populations", dnId)]);
  this.render("editDn", {
    "data": function() {
      return DarwinianNetworks.findOne(dnId);
    }
  });
}, {name: "editDn"})



import { Evolutions } from "../imports/api/evolutions.js";

Router.route("/evolution/play/:evolutionId/:dnId", function() {
  var dnId = this.params.dnId;
  var evolutionId = this.params.evolutionId;
  this.wait([Meteor.subscribe("darwinianNetwork", dnId), Meteor.subscribe("populations", dnId), Meteor.subscribe("evolution", evolutionId)]);
  this.render("playEvolution", {
    "data": function() {
      return Evolutions.findOne(evolutionId);
    }
  });
}, {name: "playEvolution"})

Router.route("/evolution/view/:evolutionId", function() {
  var evolutionId = this.params.evolutionId;
  this.wait([Meteor.subscribe("allDarwinianNetwork"), Meteor.subscribe("allPopulations"), Meteor.subscribe("evolution", evolutionId)]);
  this.render("viewEvolution", {
    "data": function() {
      return Evolutions.findOne(evolutionId);
    }
  });
}, {name: "viewEvolution"})
