export const Evolutions = new Mongo.Collection('evolutions');

Meteor.methods({
  "evolutions.insert": function(name, dns) {
    var evolutionId = Evolutions.insert({"name": name, "dns": dns, "createdAt": new Date()});
    return evolutionId;
  },
  "evolutions.updateName": function(evolutionId, name) {
    Evolutions.update(evolutionId, {$set: {"name": name}});
  },
  "evolutions.updateDns": function(evolutionId, dns) {
    Evolutions.update(evolutionId, {$set: {"dns": dns}});
  },
  "evolutions.remove": function(evolutionId) {
    // Remove Evolution DNs
    var dns = Evolutions.findOne(evolutionId).dns;
    for (var i = dns.length - 1; i >= 0; i--) {
      Meteor.call("darwiniannetworks.remove", dns[i]);
    }
    Evolutions.remove(evolutionId);
  },
});

if (Meteor.isServer) {
  Meteor.publish('allEvolutions', function() {
      return Evolutions.find({});
  });

  Meteor.publish('evolution', function(evolutionId) {
      return Evolutions.find({"_id": evolutionId});
  });
}
