export const Populations = new Mongo.Collection('populations');

Meteor.methods({
  "populations.insert": function(dnId, combative, docile, _posX, _posY) {
    var posX = (typeof _posX !== 'undefined') ?  _posX : dnLabSettings.drawWidth/2;
    var posY = (typeof _posY !== 'undefined') ?  _posY : dnLabSettings.drawHeight/2;
    var popId = Populations.insert({"dnId": dnId, "combative": combative, "docile": docile, "posX": posX, "posY": posY, "createdAt": new Date()});
    return popId;
  },
  "populations.remove": function(popId) {
    return Populations.remove(popId);
  },
  "populations.updatePos": function(popId, x, y) {
    Populations.update(popId,{$set: {"posX": x, "posY": y}});
  }
});

if (Meteor.isServer) {
  Meteor.publish('populations', function(dnId) {
      return Populations.find({"dnId": dnId});
  });

  Meteor.publish('allPopulations', function(dnId) {
      return Populations.find({});
  });
}
