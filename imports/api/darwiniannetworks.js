export const DarwinianNetworks = new Mongo.Collection('darwiniannetworks');

import { Populations } from "./populations.js";

Meteor.methods({
  "darwiniannetworks.insert": function(name, isEvolution) {
    var isEvolution = (typeof isEvolution !== 'undefined') ?  isEvolution : false;
    var dnId = DarwinianNetworks.insert({"name": name, "isEvolution": isEvolution, "createdAt": new Date()});
    return dnId;
  },
  "darwiniannetworks.remove": function(dnId) {
    Populations.remove({"dnId": dnId});
    return DarwinianNetworks.remove(dnId);
  }
});

if (Meteor.isServer) {
  Meteor.publish('allDarwinianNetworks', function() {
      return DarwinianNetworks.find({});
  });
  Meteor.publish('darwinianNetwork', function(dnId) {
      return DarwinianNetworks.find(dnId);
  });
}
