import "./drawDn.html";


drawAPop = function(draw, c, d, x, y) {
  var combative = c;
  var docile = d;
  var posX = x;
  var posY = y;

  var verticalShift = 30;
  var horizontalShift = 30;
  var traitsSize = 20;

  var maxHeight = (combative.length >= docile.length) ? combative.length : docile.length;

  var circleRadius = 100 + (traitsSize * maxHeight * 0.5);
  
  var popGroup = draw.group();
  var circle = draw.circle(circleRadius).attr({"fill": "#fff", "stroke-width": 4, "stroke": "#000", "cx": posX, "cy": posY});
  popGroup.add(circle);

  for (var i = combative.length - 1; i >= 0; i--) {
    var posXTrait = posX - horizontalShift;
    var posYTrait = posY + i*verticalShift - traitsSize;
    var c = draw.circle(20).attr({"fill": "#fff", "stroke-width": 4, "stroke": "#000", "cx": posXTrait, "cy": posYTrait});
    var t = draw.text(combative[i]).attr({"x": posXTrait, "y": posYTrait - 17, "fill": "#000"});
    popGroup.add(c);
    popGroup.add(t);
  }

  for (var i = docile.length - 1; i >= 0; i--) {
    var posXTrait = posX + horizontalShift;
    var posYTrait = posY + i*verticalShift - traitsSize;
    var c = draw.circle(traitsSize).attr({"fill": "#000", "cx": posXTrait, "cy": posYTrait});
    var t = draw.text(docile[i]).attr({"x": posXTrait, "y": posYTrait - 17, "fill": "#fff"});
    popGroup.add(c);
    popGroup.add(t);
  }

  return popGroup;
}