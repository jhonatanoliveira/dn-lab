import "./editDn.html";

import {  Template } from "meteor/templating";

import { Populations } from "../../api/populations.js"

Template.editDn.onRendered(function(){

  Tracker.autorun(function(){

    var dnId = Router.current().params._id;

    var popRadius = 50;

    $("#drawingDiv").empty();
    var draw = SVG("drawingDiv").size(dnLabSettings.drawWidth, dnLabSettings.drawHeight);

    var rec = draw.rect(dnLabSettings.drawWidth, dnLabSettings.drawHeight).attr({"fill": "#fff", "stroke-width": 4, "stroke": "#000", "stroke-dasharray": "10,10" })

    Populations.find({"dnId": dnId}).forEach(function(doc){
      var popGroup = drawAPop(draw, doc.combative, doc.docile, doc.posX, doc.posY, doc._id);

      // Events
      popGroup.data("popId", doc._id);

      popGroup.draggable();
      popGroup.on("dragend.namespace", function(e){
        Meteor.call("populations.updatePos", popGroup.data("popId"), e.detail.p.x, e.detail.p.y);
      });

      popGroup.dblclick(function(e){
        alertify.confirm("Confirm removal", "Please, confirm the removal of population.", function(){
          Meteor.call("populations.remove", popGroup.data("popId"));
        }, function(){});
      });

    });

  });

});

Template.editDn.events({
  "submit .add-pop": function(event) {
    event.preventDefault();
    var dnId = this._id;
    
    var comb = event.target.combative.value;
    var doc = event.target.docile.value;
    
    if (comb.length > 0 || doc.length > 0 ) {
      var combPre = comb.split(",").filter(function(e){ return e.length > 0; });
      var docPre = doc.split(",").filter(function(e){ return e.length > 0; });
      Meteor.call("populations.insert", dnId, combPre, docPre);

      event.target.combative.value = "";
      event.target.docile.value = "";
    }
  }
});