import "./dns.html"

import {  Template } from "meteor/templating";

import { DarwinianNetworks } from "../../api/darwiniannetworks.js"
import { Populations } from "../../api/populations.js"



Template.dns.helpers({
  
  dns: function() {
    return DarwinianNetworks.find({"isEvolution": false}, {"sort": [["createdAt", "desc"]]});
  }

});

Template.dns.events({
  
  "click .new-dn": function(event) {
    var anyError = false;
    alertify.prompt("New DN","Please, give it a name.", "",
      function(evt, value ){
        if (DarwinianNetworks.find({"name": value}).count() > 0) {
          alertify.alert("Warning","Name already taken.");
          anyError = true;
        } else {
          Meteor.call("darwiniannetworks.insert", value, function(error, result){
           if (error)  {
            alertify.alert("Error while creating DN.");
            anyError = true;
           } else {
            alertify.success("DN  '" + value + "' added");
           }
          });
        }
      },
      function(){
        alertify.error("DN creation canceled")
      });
    if (anyError) {alertify.error("DN creation canceled");}
  },

  "click .remove-dn": function(event) {
    var dnId = this._id;
    var dnName = this.name;

    alertify.confirm("DN Removal", "Please, confirm the removal of '"+dnName+"'",
      function(){
        Meteor.call("darwiniannetworks.remove", dnId);
        alertify.success("DN '"+dnName+"' removed");
      },
      function(){
        alertify.error('Removal canceled');
      });
  },

  "click .evolve-dn": function(event) {
    var dnId = this._id;
    var dnName = this.name;
    alertify.prompt("New Evolution", "Please, give a name for the Evolution of DN '"+dnName+"'.", "",
      function(evt, value){
        if (value) {
          alertify.success("Creating new Evolution...");
          // Create new DN
          Meteor.call("darwiniannetworks.insert", "evolution_"+value, true, function(error, newDnId){
            if (error) {
              alertify.alert("Internal error", "Error while creating first Darwinian network.");
            } else {
              // Copy all pops from original DN to the new one
              Populations.find({"dnId": dnId}).forEach(function(doc){
                Meteor.call("populations.insert", newDnId, doc.combative, doc.docile, doc.posX, doc.posY);
              });
              // Create evolution with new DN as being the first
              Meteor.call("evolutions.insert", value, [newDnId], function(error,result){
                if (error) {
                  alertify.alert("Internal error", "Error while creating evolution.");
                } else {
                  Router.go("playEvolution", {"evolutionId": result, "dnId": newDnId});
                }
              });
            }
          });
        } else {
          alertify.alert("No name", "Please, give it a name.");
        }
      },
      function(){
        alertify.error('Evolution canceled');
      });
  }

});