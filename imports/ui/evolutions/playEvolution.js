import "./playEvolution.html"

import {  Template } from "meteor/templating";

import { Populations } from "../../api/populations.js"
import { DarwinianNetworks } from "../../api/darwiniannetworks.js"
import { Evolutions } from "../../api/evolutions.js"

Template.playEvolution.onRendered(function(){

  Tracker.autorun(function(){

    var dnId = Router.current().params.dnId;
    var evolutionId = Router.current().params.evolutionId;

    var popRadius = 50;

    if ($("#drawingEvoDiv")){
      $("#drawingEvoDiv").empty();
      var draw = SVG("drawingEvoDiv").size(dnLabSettings.drawWidth, dnLabSettings.drawHeight);

      var rec = draw.rect(dnLabSettings.drawWidth, dnLabSettings.drawHeight).attr({"fill": "#fff", "stroke-width": 4, "stroke": "#000", "stroke-dasharray": "10,10" })

      Populations.find({"dnId": dnId}).forEach(function(doc){

        // Draw Population
        var popGroup = drawAPop(draw, doc.combative, doc.docile, doc.posX, doc.posY, doc._id);

        // Events
        popGroup.data("popId", doc._id);
        popGroup.data("dnId", doc.dnId);

        popGroup.draggable();

        // Vars used in merging and replication
        var dn = DarwinianNetworks.findOne(popGroup.data("dnId"));
        var thisPopId = popGroup.data("popId");

        // Merging
        popGroup.on("dragend.namespace", function(e){
          var x0 = e.detail.p.x;
          var y0 = e.detail.p.y;
          Meteor.call("populations.updatePos", thisPopId, x0, y0);
          // Find collision: (R0-R1)^2 <= (x0-x1)^2+(y0-y1)^2 <= (R0+R1)^2
          var otherPopulations = Populations.find({"dnId": dnId}).fetch();
          for (var i=0; i < otherPopulations.length; i++) {
            var otherPop = otherPopulations[i];
            if (otherPop._id != doc._id) {
              var x1 = otherPop.posX;
              var y1 = otherPop.posY;
              var comp = Math.pow(x0-x1, 2) + Math.pow(y0-y1, 2);
              var hasCollided = ( (0 <=  comp ) && ( comp <= Math.pow(popRadius+popRadius, 2) ) );
              if (hasCollided) {
                alertify.confirm("Merging populations", "Confirm the merge?",
                  function(){
                    Meteor.call("darwiniannetworks.insert", dn.name, true, function(error, newDnId){
                      if (!error) {
                        // Copy populations
                        Populations.find({"dnId": dn._id}).forEach(function(doc){
                          if ((doc._id != thisPopId) && (doc._id != otherPop._id)) {
                            Meteor.call("populations.insert", newDnId, doc.combative, doc.docile, doc.posX, doc.posY);
                          }
                        });
                        // Create merged population
                        var toMergePop = Populations.findOne(thisPopId);
                        var combConcated = toMergePop.combative.concat( otherPop.combative );
                        var newCombative = combConcated.filter(function (item, pos) {return combConcated.indexOf(item) == pos});
                        var docConcated = toMergePop.docile.concat( otherPop.docile );
                        var newDocile = docConcated.filter(function (item, pos) {return (docConcated.indexOf(item) == pos) && (newCombative.indexOf(item) == -1)});
                        Meteor.call("populations.insert", newDnId, newCombative, newDocile);
                        // Update evolution
                        var currentDnsEvo = Evolutions.findOne(evolutionId).dns;
                        currentDnsEvo.push(newDnId);
                        Meteor.call("evolutions.updateDns", evolutionId, currentDnsEvo);
                        // Redirect
                        Router.go("playEvolution", {"evolutionId": evolutionId, "dnId": newDnId});
                      }
                    });
                  },
                  function(){
                    alertify.error('Merge canceled');
                  });
                break;
              }
            }
          }
        });

        // Replication
        popGroup.dblclick(function(e){

          alertify.prompt("Replication", "Choose a combative subset to remove", "",
            function(evt, value ){
              // Create new DN
              var combSubset = value.split(",").filter(function(e){ return e.length > 0; });
              Meteor.call("darwiniannetworks.insert", dn.name, true, function(error, newDnId){
                if (!error) {
                  // Copy populations
                  Populations.find({"dnId": dn._id}).forEach(function(doc){
                    if (doc._id != thisPopId) {
                      Meteor.call("populations.insert", newDnId, doc.combative, doc.docile, doc.posX, doc.posY);
                    }
                  });
                  // Create new replica
                  var toReplicatePop = Populations.findOne(popGroup.data("popId"));
                  var newCombative = toReplicatePop.combative.filter(function(item){return combSubset.indexOf(item) == -1});
                  Meteor.call("populations.insert", newDnId, newCombative, toReplicatePop.docile);
                  // Update evolution
                  var currentDnsEvo = Evolutions.findOne(evolutionId).dns;
                  currentDnsEvo.push(newDnId);
                  Meteor.call("evolutions.updateDns", evolutionId, currentDnsEvo);
                  // Redirect
                  Router.go("playEvolution", {"evolutionId": evolutionId, "dnId": newDnId});
                }
              });
            },
            function(){
              alertify.error('Replication canceled');
            }
          );
        });

      });
    }
  });

});