import "./evolutions.html"

import {  Template } from "meteor/templating";

import { Evolutions } from "../../api/evolutions.js"

Template.evolutions.helpers({
  
  "evolutions": function() {
    return Evolutions.find({});
  },

  "lastElement": function(l) {
    return l[l.length-1];
  }

});

Template.evolutions.events({

  "click .edit-evolution": function(e) {
    var evoId = this._id;
    alertify.prompt("Edit Evolution", "Give the Evolution a name", this.name,
      function(evt, value ){
        Meteor.call("evolutions.updateName", evoId, value);
        alertify.success('New evolution name: ' + value);
      },
      function(){
        alertify.error('Evolution edition canceled');
      }
    );
  },

  "click .remove-evolution": function(e) {
    var evoId = this._id;
    var evoName = this.name;

    alertify.confirm("Confirm Evolution Removal", "Confirm the removal of evolution '"+evoName+"'.",
      function(){
        Meteor.call("evolutions.remove", evoId);
        alertify.success("Evolution '" + evoName + "' removed");
      },
      function(){
        alertify.error('Evolution removal canceled');
      }
    );

  }

});