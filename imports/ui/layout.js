import { Meteor } from 'meteor/meteor'
import { Template } from "meteor/templating";

import "./layout.html"

Template.layout.events({

  "click .toggle-menu": function(event) {
    $('.ui.sidebar').sidebar('toggle');
  },

  "click .ui.sidebar .item": function(event) {
    $('.ui.sidebar').sidebar('toggle'); 
  }

})